﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreCounterView : MonoBehaviour
{
    [SerializeField] private Text _text;
    private int count;
    
    public void UpdateCounter()
    {
        ++count;
        _text.text = count.ToString();
    }
}
