﻿using UnityEngine;

[CreateAssetMenu(menuName = "GarraTeam/RandomNumberGeneratorModel", fileName = "RandomNumberGeneratorModel")]
public class RandomNumberGeneratorModel : ScriptableObject
{
    [SerializeField] float rangeMin, rangeMax;

    public double StartGeneration()
    {
        var result = Random.Range(rangeMin, rangeMax + 1);
            return System.Math.Truncate(result);
    }
}
