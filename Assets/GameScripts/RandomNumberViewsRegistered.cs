﻿using com.FDT.Common;
using com.FDT.Common.Registrables;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GarraTeam/RandomNumberViewsProvider", fileName = "RandomNumberViewsProvider")]
public class RandomNumberViewsRegistered : ScriptableObject, IRegisterer<NumberSelectionView, IDAsset>
{
    [SerializeField] private List<NumberSelectionView> _numberSelectionViews;

    public List<NumberSelectionView> NumberSelectionViews { get => _numberSelectionViews; }

    public bool Register(NumberSelectionView registeredItem)
    {
        if (!_numberSelectionViews.Contains(registeredItem))
        {
            _numberSelectionViews.Add(registeredItem);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool Unregister(NumberSelectionView registeredItem)
    {
        if (_numberSelectionViews.Contains(registeredItem))
        {
            _numberSelectionViews.Remove(registeredItem);
            return true;
        }
        else
        {
            return false;
        }
    }
}
