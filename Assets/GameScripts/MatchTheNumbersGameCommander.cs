﻿using com.FDT.GameEvents;
using System;
using System.Threading.Tasks;
using UnityEngine;

public class MatchTheNumbersGameCommander : MonoBehaviour
{
    [SerializeField] private GameEvent StartGeneration;
    [SerializeField] private GameEvent HideNumberInLetters;
    [SerializeField] private GameEvent ShowNumberSelections;

    [SerializeField] private GameEventHandle OnReStartRound;

    private void OnEnable()
    {
        OnReStartRound.AddEventListener(StartGame);
    }

    private void OnDisable()
    {
        OnReStartRound.RemoveEventListener(StartGame);
    }

    private void Start()
    {
        StartGame();
    }

    private async void StartGame()
    {
        StartGeneration.Raise();
        await Task.Delay(TimeSpan.FromSeconds(4));
        HideNumberInLetters.Raise();
        ShowNumberSelections.Raise();
    }
}
