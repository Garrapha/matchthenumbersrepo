﻿using com.FDT.Common;
using com.FDT.Common.CustomGameEvents;
using com.FDT.GameEvents;
using System.Collections.Generic;
using System.Linq;
using GarraTeam.Core;
using UnityEngine;

public class RandomNumberController : MonoBehaviour
{
    [Header("Incomming Events")]
    [SerializeField] private GameEventHandle StartGeneration;
    [SerializeField] private GameEventHandle HideNumberInLetters;
    [SerializeField] private StringGameEvent SendRandomNumberToDialogue;
    [SerializeField] private GameEventHandle ShowNumberSelections;
    [SerializeField] private IDAssetGameEventHandle OnSelectedView;
    [Header("Sending Events")]
    [SerializeField] private GameEvent OnRightAttempt;
    [SerializeField] private GameEvent OnWrongAttempt;
    [SerializeField] private GameEvent OnReStartRound;
    [Header("Data")]
    [SerializeField] private RandomNumberGeneratorModel _generatorModel;
    [SerializeField] private RandomNumberViewsRegistered _viewsRegistered;
    [SerializeField] private NumberToLetterConverterSpanish _numberToLetterConverter;
    [SerializeField] private IDAsset _NumerInLetterView;
    [SerializeField] private List<IDAsset> _viewsIds;

    private double _randomNumber;
    private IDAsset _rightSelectionViewId;
    private int _attempts;

    private void OnEnable()
    {
        StartGeneration.AddEventListener(GenerateNewNumber);
        HideNumberInLetters.AddEventListener(DoHideNumberInLetters);
        ShowNumberSelections.AddEventListener(SendRightAndWrongNumbersToView);
        OnSelectedView.AddEventListener(ReactToSelection);
    }

    private void OnDisable()
    {
        HideNumberInLetters.RemoveEventListener(DoHideNumberInLetters);
        StartGeneration.RemoveEventListener(GenerateNewNumber);
        ShowNumberSelections.RemoveEventListener(SendRightAndWrongNumbersToView);
        OnSelectedView.RemoveEventListener(ReactToSelection);
    }

    private void GenerateNewNumber()
    {
        _attempts = 2;
        _randomNumber = _generatorModel.StartGeneration();
        var rightNumberInLetters = _numberToLetterConverter.NumeroALetras(_randomNumber.ToString());

        UIManager.Instance.Show(_NumerInLetterView.name, 
            () =>
        {
            SendRandomNumberToDialogue.Raise(rightNumberInLetters);
        },
            ()=>
        {
            
        }); 
    }

    private void DoHideNumberInLetters()
    {
        UIManager.Instance.Close(_NumerInLetterView.name);
    }

    private void SendRightAndWrongNumbersToView()
    {
        List<IDAsset> emptyViews = new List<IDAsset>(_viewsIds);
        bool sendRight = false;
        
        while (emptyViews.Count != 0)
        {
            var rndViewIdx = Random.Range(0, emptyViews.Count);

            if (!sendRight)
            {
                SendNumberToView(_randomNumber, emptyViews[rndViewIdx]);
                _rightSelectionViewId = emptyViews[rndViewIdx];
                sendRight = true;
            }
            else
            {
                var wrongNumber = _generatorModel.StartGeneration();
                SendNumberToView(wrongNumber, emptyViews[rndViewIdx]);
            }

            emptyViews.RemoveAt(rndViewIdx);
        }
    }

    private void SendNumberToView(double number, IDAsset viewID)
    {
        UIManager.Instance.Show(viewID.name, 
            () => 
        {
            NumberSelectionView activeView = _viewsRegistered.NumberSelectionViews.FirstOrDefault(t => t.referenceID == viewID);
            activeView.SetText(number.ToString());
            activeView.SetInteractable(false);
        },
            ()=>
        {
            NumberSelectionView activeView = _viewsRegistered.NumberSelectionViews.FirstOrDefault(t => t.referenceID == viewID);
            activeView.SetInteractable(true);
        });
    }
    
    private void ReactToSelection(IDAsset selectionID)
    {
        NumberSelectionView selected = _viewsRegistered.NumberSelectionViews.FirstOrDefault(t => t.referenceID == selectionID);

        if (selected.referenceID == _rightSelectionViewId)
        {
            selected.DoRightAnimation();
            DoHideNumberSelections();
            OnRightAttempt.Raise();
            OnReStartRound.Raise();
            
        }
        else if (_attempts != 0)
        {
            selected.DoWrongAnimation();
            selected.SetInteractable(false);
            UIManager.Instance.Close(selected.referenceID.name);
            --_attempts;
            OnWrongAttempt.Raise();
        }

        if (_attempts != 0) return;
        
        var rightSelection = _viewsRegistered.NumberSelectionViews.FirstOrDefault(t => t.referenceID == _rightSelectionViewId);
        
        rightSelection.DoRightAnimation();
        selected.DoWrongAnimation();

        DoHideNumberSelections();
        OnWrongAttempt.Raise();
        OnReStartRound.Raise();
    }
    
    private void DoHideNumberSelections()
    {
        foreach (var selection in _viewsRegistered.NumberSelectionViews)
        {
            selection.SetInteractable(false);
            UIManager.Instance.Close(selection.referenceID.name);
        }
    }
}
