﻿using com.FDT.Common;
using GarraTeam.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "SceneManagerAsset", menuName = "Asset/Managers")]
public class ScriptableSceneManager : SingletonScriptableObject<ScriptableSceneManager>
{
    [SerializeField] private IDAssetGameEventHandle _sceneLoadRequest;

    private void OnEnable()
    {
        _sceneLoadRequest.AddEventListener(LoadScene);
    }

    private void LoadScene(IDAsset sceneId)
    {
        SceneManager.LoadSceneAsync(sceneId.name); 
    }

    public override void OnDynamicCreation()
    {

    }

    private void OnDisable()
    {
        _sceneLoadRequest.RemoveEventListener(LoadScene);
    }
}
