﻿using com.FDT.Common;
using System;
using UnityEngine;

[Serializable]
public class UIData
{
    public IDAsset id;
    public Animator animator;
    public string _in;
    public string _out;
}


