﻿using com.FDT.Common;
using com.FDT.GameEvents;
using GarraTeam.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] GameEventHandle BackButtonGameEvent;
    [SerializeField] List<GameObject> uiDatasProviderPrefabs;

    List<UIComponent> uiDatasProvider = new List<UIComponent>();
    List<UIComponent> _cacheDatas = new List<UIComponent>();
    UIComponent _currentUIData;

    public void Init()
    {
        OnDynamicCreation();
    }

    private void OnEnable()
    {
        foreach (var p in uiDatasProviderPrefabs)
        {
            uiDatasProvider.Add(p.GetComponent<UIComponent>());
        }

        BackButtonGameEvent.AddEventListener(HandleBackButtonEvent);
    }

    public void Disable()
    {
        uiDatasProvider.Clear();
        BackButtonGameEvent.RemoveEventListener(HandleBackButtonEvent);
    }

    private void HandleBackButtonEvent()
    {
        Delete(_currentUIData.UiData.id.name);
    }

    private bool DataExist(string id, out UIComponent component)
    {
        component = uiDatasProvider.Find(x => x.UiData.id.name == id);
        return component != null;
    }

    private bool CacheExist(string id, out UIComponent cacheData)
    {
        cacheData = _cacheDatas.Find(x=> x.UiData.id.name == id);
        return cacheData != null;
    }

    public void Show(string id, Action onEnabled, Action onAnimationFinish)
    {
        if (CacheExist(id, out UIComponent cacheData))
        {
            cacheData.gameObject.SetActive(true);
            onEnabled.Invoke();
            cacheData.AnimIn(onAnimationFinish.Invoke);
            _currentUIData = cacheData;
            return;
        }

        if (!DataExist(id, out UIComponent data)) return;

        var clone = Instantiate(data, transform);
        clone.gameObject.SetActive(true);
        onEnabled.Invoke();
        clone.AnimIn(onAnimationFinish.Invoke);
        _currentUIData = clone;

        if (_cacheDatas.Contains(clone/*data*/)) return;

        clone.CloneGameObject = clone.gameObject;
        _cacheDatas.Add(clone/*data*/);
    }

    public void Close(string id)
    {
        if (!CacheExist(id, out UIComponent cacheData)) return;
        
        cacheData.AnimOut(() => 
        {
            cacheData.CloneGameObject.SetActive(false);
        });


        var cacheIdx = _cacheDatas.FindLastIndex(x => x.CloneGameObject.activeInHierarchy);
        if (cacheIdx != -1) {
            _currentUIData = _cacheDatas[cacheIdx]; 
        }
    }

    public void Delete(string id)
    {
        if (!DataExist(id, out UIComponent cacheData)) return;

        var cacheIdx = _cacheDatas.FindLastIndex(x => x.CloneGameObject.activeInHierarchy);
        if (cacheIdx != -1)
        {
            _currentUIData = _cacheDatas[cacheIdx];
        }

        _cacheDatas.Remove(cacheData);

        _currentUIData.AnimOut(() =>
        {
            Destroy(cacheData.CloneGameObject);
        });
    }
}
