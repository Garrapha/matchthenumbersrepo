﻿using com.FDT.Common;
using com.FDT.Common.Registrables;
using UnityEngine;
using UnityEngine.UI;

public class NumberSelectionView : UIComponent, IRegistrable<IDAsset>
{
    [SerializeField] private Text _text;
    [SerializeField] private Button _button;
    [SerializeField] private RandomNumberViewsRegistered _viewsRegistered;

    public IDAsset referenceID => Id;

    protected override void OnEnable()
    {
        base.OnEnable();
        _viewsRegistered.Register(this);
    }

    private void OnDisable()
    {
        _viewsRegistered.Unregister(this);
    }

    public void SetText(string text)
    {
        _text.text = text;
        GetComponent<Image>().color = Color.white;
    }

    public void DoRightAnimation()
    {
        GetComponent<Image>().color = Color.green;
    }

    public void DoWrongAnimation()
    {
        GetComponent<Image>().color = Color.red;
    }

    public void SetInteractable(bool b)
    {
        _button.interactable = b;
    }
}
