﻿using com.FDT.Common;
using System;
using System.Collections;
using UnityEngine;

public class UIComponent : MonoBehaviour
{
    [SerializeField] protected UIData uiData;
    [SerializeField] protected AnimationClip outAnimation;
    [SerializeField] protected AnimationClip inAnimation;

    public UIData UiData
    {
        get
        {
            return uiData;
        }
        set
        {
            uiData = value;
        }
    }

    public GameObject CloneGameObject { get; set; }
    public IDAsset Id => uiData.id;

    protected virtual void OnEnable()
    {
        uiData.animator.SetTrigger(uiData._in);
    }

    public void AnimOut(Action onFinish)
    {
        StartCoroutine(WaitAnimation(onFinish, uiData._out, outAnimation));
    }

    IEnumerator WaitAnimation(Action onFinish, string trigger, AnimationClip animationInfo)
    {
        uiData.animator.SetTrigger(trigger);
        yield return new WaitForSeconds(animationInfo.length);
        onFinish.Invoke();
    }

    public void AnimIn(Action onFinish)
    {
        StartCoroutine(WaitAnimation(onFinish, uiData._in, inAnimation));
    }
}
