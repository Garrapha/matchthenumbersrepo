using UnityEditor;
using com.FDT.Common;
using com.FDT.GameEvents.Editor;

namespace GarraTeam.Core.Editor
{
	[CustomEditor(typeof(IDAssetGameEvent))]
	public class IDAssetGameEventEditor : GameEvent1Editor<IDAsset>
	{
	}
}
