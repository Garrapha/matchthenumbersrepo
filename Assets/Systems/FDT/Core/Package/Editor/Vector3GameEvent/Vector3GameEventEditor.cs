using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Common.CustomGameEvents.Editor
{
	[CustomEditor(typeof(Vector3GameEvent))]
	public class Vector3GameEventEditor : GameEvent1Editor<Vector3>
	{
	}
}
