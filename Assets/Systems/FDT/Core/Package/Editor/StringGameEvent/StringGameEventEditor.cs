using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Common.CustomGameEvents.Editor
{
	[CustomEditor(typeof(StringGameEvent))]
	public class StringGameEventEditor : GameEvent1Editor<string>
	{
	}
}
