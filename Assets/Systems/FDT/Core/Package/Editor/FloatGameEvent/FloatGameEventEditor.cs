using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Common.CustomGameEvents.Editor
{
	[CustomEditor(typeof(FloatGameEvent))]
	public class FloatGameEventEditor : GameEvent1Editor<float>
	{
	}
}
