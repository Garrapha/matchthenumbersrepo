using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Common.CustomGameEvents.Editor
{
	[CustomEditor(typeof(IntGameEvent))]
	public class IntGameEventEditor : GameEvent1Editor<int>
	{
	}
}
