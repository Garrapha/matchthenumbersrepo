using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Common.CustomGameEvents.Editor
{
    [CustomEditor(typeof(Vector2GameEvent))]
    public class Vector2GameEventEditor : GameEvent1Editor<Vector2>
    {
    }
}
