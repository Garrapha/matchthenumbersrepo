using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Common.CustomGameEvents.Editor
{
	[CustomEditor(typeof(BoolGameEvent))]
	public class BoolGameEventEditor : GameEvent1Editor<bool>
	{
	}
}
