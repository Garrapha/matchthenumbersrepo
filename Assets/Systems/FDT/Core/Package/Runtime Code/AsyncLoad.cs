﻿using System;
using UnityEngine;

    public class AsyncLoad : CustomYieldInstruction
    {
        protected bool _isDone = false;
        public event Action OnComplete;
        public virtual bool isDone
        {
            get { return _isDone; }
            set
            {
                _isDone = value;
                if (_isDone && OnComplete != null)
                {
                    OnComplete.Invoke();
                }
            }
        }

        public override bool keepWaiting
        {
            get { return !isDone; }
        }
    }