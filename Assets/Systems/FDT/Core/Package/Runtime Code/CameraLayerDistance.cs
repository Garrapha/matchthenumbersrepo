﻿using UnityEngine;

	/*
	 * 		Contributors:	Franco Scigliano
	 * 		Description:	
	 */
	[ExecuteInEditMode]
	public class CameraLayerDistance : MonoBehaviour {
		
		#region Classes
		#endregion
		
		#region GameEvents
		//[Header("GameEvents"), SerializeField] 
		#endregion
		
		#region Actions
		#endregion
		
		#region UnityEvents
		//[Header("UnityEvents"), SerializeField] 
		#endregion
		
		#region Fields
		[Header("Fields"), SerializeField] protected float[] _layerDistances = new float[32];
		[SerializeField, UnityEngine.Serialization.FormerlySerializedAs("cam")] protected Camera _cam;
		#endregion

		#region Properties

		#endregion

		#region Methods

		private void Start()
		{
			_cam.layerCullDistances = _layerDistances;
		}
		#if UNITY_EDITOR
		private void Update()
		{
			_cam.layerCullDistances = _layerDistances;
		}
		#endif

		#endregion
	}
