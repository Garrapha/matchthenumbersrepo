using UnityEngine.Events;
using com.FDT.Common;
using com.FDT.GameEvents;

namespace GarraTeam.Core
{
	public class IDAssetGameEventListener : GameEvent1Listener<IDAsset, IDAssetGameEvent, IDAssetGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<IDAsset>
		{
		}
	}
}
