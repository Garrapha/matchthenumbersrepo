using UnityEngine;
using com.FDT.GameEvents;
using com.FDT.Common;

namespace GarraTeam.Core
{
	[CreateAssetMenu(menuName = "GarraTeam/GameEvents/IDAssetGameEvent")]
	public class IDAssetGameEvent : GameEvent1<IDAsset>
	{
		public override string arg0label
		{
			get { return "id"; }
		}
	}
}
