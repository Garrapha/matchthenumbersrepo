﻿using UnityEngine;

    public class GenericVariableAsset<T> : ScriptableObject where T : struct
    {
        [SerializeField] protected T defaultValue;
        protected T currentValue;

        public virtual T CurrentValue
        {
            get => currentValue;
            set => currentValue = value;
        }

        private void OnEnable()
        {
            Reset();
        }

        public void Reset()
        {
            CurrentValue = defaultValue;
        }

        public override string ToString()
        {
            return $"[{GetType().Name}] {name}: {currentValue}";
        }
    }