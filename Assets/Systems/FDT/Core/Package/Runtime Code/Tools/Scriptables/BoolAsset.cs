﻿using UnityEngine;

    [CreateAssetMenu(menuName ="FDT/Core/Assets/Bool")]
    public class BoolAsset : GenericVariableAsset<bool>
    {

    }