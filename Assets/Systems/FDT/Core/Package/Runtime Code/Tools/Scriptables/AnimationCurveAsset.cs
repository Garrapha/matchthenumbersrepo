﻿using UnityEngine;

    /*
     * 		Contributors:	Franco Scigliano
     * 		Description:	
     */
    [CreateAssetMenu(menuName = "FDT/Assets/Animation Curve", fileName = "AnimationCurveAsset")]
public class AnimationCurveAsset : ScriptableObject
{
	[HelpBox("Keep curve time from 0 to 1.", HelpBoxMessageType.Info)]
    #region Classes
    #endregion
		
    #region GameEvents
    //[Header("GameEvents"), SerializeField] 
    #endregion
		
    #region Actions
    #endregion
		
    #region UnityEvents
    //[Header("UnityEvents"), SerializeField] 
    #endregion
		
    #region Fields
    //[Header("Fields"), SerializeField] 
    
    [SerializeField] private AnimationCurve curve;
    [SerializeField, Tooltip("This value will multiply curve time")] private float time = 1f;
    #endregion

    #region Properties
    public AnimationCurve Curve => curve;

    public float Time => time;

    #endregion

    #region Methods
		
    #endregion
}
