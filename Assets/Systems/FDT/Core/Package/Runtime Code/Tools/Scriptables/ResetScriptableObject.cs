﻿using UnityEngine;

    public class ResetScriptableObject: ScriptableObject
    {
        public virtual void ResetData() { }
    }