using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/BoolGameEvent")]
	public class BoolGameEvent : GameEvent1<bool>
    {
	}
}
