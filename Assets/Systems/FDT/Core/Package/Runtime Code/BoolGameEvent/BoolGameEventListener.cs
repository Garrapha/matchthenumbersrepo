using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.Common.CustomGameEvents
{
	public class BoolGameEventListener : GameEvent1Listener<bool, BoolGameEvent, BoolGameEventListener.BoolUEvent>
	{
        [System.Serializable]
        public class BoolUEvent : UnityEvent<bool>
        {
        }
	}
}
