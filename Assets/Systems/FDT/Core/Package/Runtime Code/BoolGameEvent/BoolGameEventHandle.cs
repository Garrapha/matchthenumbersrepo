using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[System.Serializable]
	public class BoolGameEventHandle: GameEvent1Handle<bool, BoolGameEvent>
	{
	}
}
