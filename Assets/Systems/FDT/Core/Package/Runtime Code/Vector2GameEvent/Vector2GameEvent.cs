using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/Vector2GameEvent")]
	public class Vector2GameEvent : GameEvent1<Vector2>
	{
	}
}
