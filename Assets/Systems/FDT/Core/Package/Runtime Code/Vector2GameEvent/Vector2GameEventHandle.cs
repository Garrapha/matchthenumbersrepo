using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[System.Serializable]
	public class Vector2GameEventHandle: GameEvent1Handle<Vector2, Vector2GameEvent>
	{
	}
}
