using com.FDT.GameEvents;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.Common.CustomGameEvents
{
	public class Vector2GameEventListener : GameEvent1Listener<Vector2, Vector2GameEvent, Vector2GameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<Vector2>
        {
        }
	}
}
