using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/Vector3GameEvent")]
	public class Vector3GameEvent : GameEvent1<Vector3>
	{
		
	}
}
