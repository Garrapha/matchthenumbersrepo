using com.FDT.GameEvents;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.Common.CustomGameEvents
{
	public class Vector3GameEventListener : GameEvent1Listener<Vector3, Vector3GameEvent, Vector3GameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<Vector3>
        {
        }
	}
}
