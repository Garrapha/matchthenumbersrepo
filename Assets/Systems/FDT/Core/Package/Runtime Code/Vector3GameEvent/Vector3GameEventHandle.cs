using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[System.Serializable]
	public class Vector3GameEventHandle: GameEvent1Handle<Vector3, Vector3GameEvent>
	{
	}
}
