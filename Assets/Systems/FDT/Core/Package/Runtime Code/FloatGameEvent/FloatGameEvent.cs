using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/FloatGameEvent")]
	public class FloatGameEvent : GameEvent1<float>
	{
	}
}
