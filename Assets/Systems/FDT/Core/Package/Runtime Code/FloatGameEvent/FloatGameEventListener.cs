using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.Common.CustomGameEvents
{
	public class FloatGameEventListener : GameEvent1Listener<float, FloatGameEvent, FloatGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<float>
        {
        }
	}
}
