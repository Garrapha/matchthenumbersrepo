using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.Common.CustomGameEvents
{
	public class StringGameEventListener : GameEvent1Listener<string, StringGameEvent, StringGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<string>
        {
        }
	}
}
