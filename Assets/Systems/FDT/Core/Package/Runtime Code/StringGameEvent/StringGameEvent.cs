using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/StringGameEvent")]
	public class StringGameEvent : GameEvent1<string>
	{
	}
}
