using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.Common.CustomGameEvents
{
	public class IntGameEventListener : GameEvent1Listener<int, IntGameEvent, IntGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<int>
        {
        }
	}
}
