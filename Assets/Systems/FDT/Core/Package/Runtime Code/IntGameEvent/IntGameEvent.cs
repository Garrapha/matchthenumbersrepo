using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Common.CustomGameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/IntGameEvent")]
	public class IntGameEvent : GameEvent1<int>
	{
	}
}
