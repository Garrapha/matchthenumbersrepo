﻿using com.FDT.Common;
using UnityEngine;

public abstract class SingletonScriptableObject<T> : ScriptableObject, ISingleton where T : ScriptableObject, ISingleton
{
    public static bool Exists { get; }
    public static T Instance { get; set; }
    public abstract void OnDynamicCreation();
}
