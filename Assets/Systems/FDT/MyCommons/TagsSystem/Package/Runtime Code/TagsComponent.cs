using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace GarraTeam.TagsSystem {
	/*
	 * 		Contributors:	Iván Cerezo
	 * 		Description:
	 */
	public class TagsComponent : MonoBehaviour {

		#region Classes
		#endregion

		#region GameEvents
		[FormerlySerializedAs("OnTagsChanged")] [Header("GameEvents"), SerializeField] protected TagsGameEventHandle OnTagsChanged;
		#endregion

		#region Actions
		#endregion

		#region UnityEvents
		//[Header("UnityEvents"), SerializeField]
		#endregion

		#region Fields
		[Header("Fields"), SerializeField] protected List<TagAsset> tagAssets = new List<TagAsset>();

		[SerializeField] protected TagManagerAsset manager;
		protected bool usable = false;

		#endregion

		#region Properties

		/// <summary>
		/// Returns a copy of tagAssetsList
		/// </summary>
		public List<TagAsset> Tags => new List<TagAsset>(tagAssets);

		public virtual bool Usable
		{
			get => usable;
			protected set => gameObject.SetActive(usable = value);
		}

		#endregion

		#region Methods

		protected void OnEnable()
		{
			SetUsability();
		}

		protected void Awake()
		{
			OnTagsChanged.AddEventListener(OnTagsChangedReaction);
		}

		private void OnDestroy()
		{
			OnTagsChanged.RemoveEventListener(OnTagsChangedReaction);
		}

		protected void SetUsability()
		{
			Usable = manager == null || manager.IsTagComponentUsable(this);
		}

		protected void OnTagsChangedReaction(List<TagAsset> tags)
		{
			if (tags == null)
			{
				// TODO : resolved that if tags is null, the desired behaviour is to call SetUsability
				SetUsability();
				return;
			}

			bool result = false;
			for (int i = 0; i < tags.Count; i++)
			{
				if (OnTagChangedReaction(tags[i]))
				{
					result = true;
				}
			}
			if (result)
				SetUsability();
		}
		protected bool OnTagChangedReaction(TagAsset tag)
		{
			if (!tagAssets.Contains(tag) && tag != null)
				return false;

			return true;
		}
		public void AddTag(TagAsset tag)
		{
			if(tagAssets.Contains(tag))
				return;

			tagAssets.Add(tag);
			SetUsability();
		}

		public void RemoveTag(TagAsset tag)
		{
			if(!tagAssets.Contains(tag))
				return;

			tagAssets.Remove(tag);
			SetUsability();
		}

		public void ClearTags()
		{
			tagAssets.Clear();
		}
		#endregion

	}
}
