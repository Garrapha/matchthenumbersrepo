﻿using com.FDT.GameEvents;
using System.Collections.Generic;
using UnityEngine;

namespace GarraTeam.TagsSystem
{
	/*
	 * 		Contributors:	Iván Cerezo
	 * 		Description:	
	 */
	[CreateAssetMenu(menuName = "GarraTeam/TagsSystem/Tag Manager Asset", fileName = "TagManagerAsset")]
	public class TagManagerAsset : ResetScriptableObject
	{

		#region Classes

		#endregion

		#region GameEvents
		[Header("Multi Tag GameEvents"), SerializeField] protected TagsGameEventHandle OnTagsDeactivationRequest;
		[SerializeField] protected TagsGameEventHandle OnTagsActivationRequest;
		[SerializeField] protected TagsGameEventHandle OnRemoveTagsFromActivesRequest;
		[SerializeField] protected TagsGameEventHandle OnRemoveTagsFromInactivesRequest;
		
		[SerializeField] protected TagsGameEventHandle AddTagsToDefaultEnabledEvent;
		[SerializeField] protected TagsGameEventHandle AddTagsToDefaultDisabledEvent;
		[SerializeField] protected TagsGameEventHandle RemoveTagsFromDefaultEnabledEvent;
		[SerializeField] protected TagsGameEventHandle RemoveTagsFromDefaultDisabledEvent;
		[SerializeField] protected GameEventHandle OnResetTagRequest;

		[Space(10), SerializeField] protected TagsGameEvent OnTagsChanged;
		#endregion

		#region Actions

		#endregion

		#region UnityEvents

		//[Header("UnityEvents"), SerializeField] 

		#endregion

		#region Fields

		[Header("Fields"), SerializeField] protected List<TagAsset> defaultEnabledTagAssets = new List<TagAsset>();
		[SerializeField] protected List<TagAsset> defaultDisabledTagAssets = new List<TagAsset>();
		protected List<TagAsset> enabledTagAssets = new List<TagAsset>();
		protected List<TagAsset> disabledTagAssets = new List<TagAsset>();

		#endregion

		#region Properties

		#endregion

		#region Methods

#if UNITY_EDITOR
		public override void ResetData()
		{
			base.ResetData();
			enabledTagAssets.Clear();
			disabledTagAssets.Clear();
		}
#endif
		public void Init()
		{
		    ClearTags();
		    
            OnTagsActivationRequest.AddEventListener(AddTagsToEnabled);
            OnTagsDeactivationRequest.AddEventListener(AddTagsToDisabled);

            OnRemoveTagsFromActivesRequest.AddEventListener(RemoveTagsFromEnabled);
            OnRemoveTagsFromInactivesRequest.AddEventListener(RemoveTagsFromDisabled);

            AddTagsToDefaultEnabledEvent.AddEventListener(AddTagsToDefaultEnabled);
            AddTagsToDefaultDisabledEvent.AddEventListener(AddTagsToDefaultDisabled);

            RemoveTagsFromDefaultEnabledEvent.AddEventListener(RemoveTagsFromDefaultEnabled);
            RemoveTagsFromDefaultDisabledEvent.AddEventListener(RemoveTagsFromDefaultDisabled);
            
            OnResetTagRequest.AddEventListener(ClearTags);      	
		}
		
		private void OnDisable()
		{
			OnTagsActivationRequest.RemoveEventListener(AddTagsToEnabled);
			OnTagsDeactivationRequest.RemoveEventListener(AddTagsToDisabled);
			
			OnRemoveTagsFromActivesRequest.RemoveEventListener(RemoveTagsFromEnabled);
			OnRemoveTagsFromInactivesRequest.RemoveEventListener(RemoveTagsFromDisabled);
			
			AddTagsToDefaultEnabledEvent.RemoveEventListener(AddTagsToDefaultEnabled); 
			AddTagsToDefaultDisabledEvent.RemoveEventListener(AddTagsToDefaultDisabled);
			
			RemoveTagsFromDefaultEnabledEvent.RemoveEventListener(RemoveTagsFromDefaultEnabled);
			RemoveTagsFromDefaultDisabledEvent.RemoveEventListener(RemoveTagsFromDefaultDisabled);

			OnResetTagRequest.RemoveEventListener(ClearTags);

			ClearTags();
		}

		protected virtual void AddTagsToEnabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				AddTagToEnabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}

		protected virtual void AddTagToEnabled(TagAsset tag)
		{
			if (tag == null || enabledTagAssets.Contains((tag)))
				return;
			
			if (disabledTagAssets.Contains(tag))
				disabledTagAssets.Remove(tag);
			
			enabledTagAssets.Add(tag);
		}

		protected virtual void AddTagsToDisabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				AddTagToDisabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}
		protected virtual void AddTagToDisabled(TagAsset tag)
		{
			if (tag == null || disabledTagAssets.Contains((tag)))
				return;
			
			if (enabledTagAssets.Contains(tag))
				enabledTagAssets.Remove(tag);
			
			disabledTagAssets.Add(tag);
		}

		protected virtual void RemoveTagsFromEnabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				RemoveTagFromEnabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}
		
		protected virtual void RemoveTagFromEnabled(TagAsset tag)
		{
			RemoveTagFromList(tag, enabledTagAssets);
		}

		protected virtual void RemoveTagsFromDisabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				RemoveTagFromDisabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}
		
		protected virtual void RemoveTagFromDisabled(TagAsset tag)
		{
			RemoveTagFromList(tag, disabledTagAssets);
		}

		protected virtual void AddTagsToDefaultEnabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				AddTagToDefaultEnabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}
		protected virtual void AddTagToDefaultEnabled(TagAsset tag)
		{
			if (tag == null || defaultEnabledTagAssets.Contains((tag)))
				return;
			
			if (defaultDisabledTagAssets.Contains(tag))
				defaultDisabledTagAssets.Remove(tag);
			
			defaultEnabledTagAssets.Add(tag);
			AddTagToEnabled(tag);
		}

		protected virtual void AddTagsToDefaultDisabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				AddTagToDefaultDisabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}

		protected virtual void AddTagToDefaultDisabled(TagAsset tag)
		{
			if (tag == null || defaultDisabledTagAssets.Contains((tag)))
				return;
			
			if (defaultEnabledTagAssets.Contains(tag))
				defaultEnabledTagAssets.Remove(tag);
			
			defaultDisabledTagAssets.Add(tag);
			AddTagToDisabled(tag);
		}

		protected virtual void RemoveTagsFromDefaultEnabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				RemoveTagFromDefaultEnabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}

		protected virtual void RemoveTagFromDefaultEnabled(TagAsset tag)
		{
			RemoveTagFromList(tag, defaultEnabledTagAssets);
			RemoveTagFromEnabled(tag);
		}

		protected virtual void RemoveTagsFromDefaultDisabled(List<TagAsset> tags)
		{
			for (int i = 0; i < tags.Count; i++)
			{
				RemoveTagFromDefaultDisabled(tags[i]);
			}
			OnTagsChanged.Raise(tags);
		}

		protected virtual void RemoveTagFromDefaultDisabled(TagAsset tag)
		{
			RemoveTagFromList(tag, defaultDisabledTagAssets);
			RemoveTagFromDisabled(tag);
		}

		protected virtual void RemoveTagFromList(TagAsset tag, List<TagAsset> list)
		{
			if (tag == null || !list.Contains((tag)))
				return;
			
			list.Remove(tag);
			//OnTagsChanged.Raise(tags);
		}


		public bool IsTagEnabled(TagAsset tag)
		{
			return enabledTagAssets.Contains(tag) && !disabledTagAssets.Contains(tag);
		}
		
		public bool IsTagDisabled(TagAsset tag)
		{
			return disabledTagAssets.Contains(tag) && !enabledTagAssets.Contains(tag);
		}

		public bool IsTagComponentUsable(TagsComponent taggedObject)
		{
			if (taggedObject.Tags.Count <= 0)
			{
				Debug.LogWarning(string.Format(
					"{0} tagsComponent has not tags to be checked so it will NOT be usable. Check if this component is meaningful.",
					taggedObject.gameObject));
				
				return false;
			}
			
			bool active = false;

			for (int i = 0; i < taggedObject.Tags.Count; i++)
			{
				if (IsTagDisabled(taggedObject.Tags[i])) return false;
				
				if (IsTagEnabled(taggedObject.Tags[i])) active = true;
			}

			return active;
		}

		public void ClearTags()
		{
			enabledTagAssets.Clear();
			disabledTagAssets.Clear();
			
			enabledTagAssets.AddRange(defaultEnabledTagAssets);
			disabledTagAssets.AddRange(defaultDisabledTagAssets);
			
			OnTagsChanged.Raise(null);
		}

		#endregion
	}
}