﻿using UnityEngine;
using GarraTeam.Core;
using com.FDT.Common;

namespace GarraTeam.TagsSystem {
	/*
	 * 		Contributors:	Franco Scigliano
	 * 		Description:	
	 */
	[CreateAssetMenu(menuName = "GarraTeam/TagsSystem/TagAsset", fileName = "TagAsset")]
	public class TagAsset : IDAsset {
		
		#region Classes
		#endregion
		
		#region GameEvents
		//[Header("GameEvents"), SerializeField] 
		#endregion
		
		#region Actions
		#endregion
		
		#region UnityEvents
		//[Header("UnityEvents"), SerializeField] 
		#endregion
		
		#region Fields
		//[Header("Fields"), SerializeField] 
		#endregion

		#region Properties

		#endregion

		#region Methods
		
		#endregion
	}
}
