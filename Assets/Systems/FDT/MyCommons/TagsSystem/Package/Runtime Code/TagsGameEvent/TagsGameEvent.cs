using UnityEngine;
using System.Collections.Generic;
using com.FDT.GameEvents;

namespace GarraTeam.TagsSystem
{
	[CreateAssetMenu(menuName = "GarraTeam/GameEvents/TagsSystem/UITagsGameEvent")]
	public class TagsGameEvent : GameEvent1<List<TagAsset>>
	{
		public override string arg0label
		{
			get { return "tags"; }
		}
	}
}
