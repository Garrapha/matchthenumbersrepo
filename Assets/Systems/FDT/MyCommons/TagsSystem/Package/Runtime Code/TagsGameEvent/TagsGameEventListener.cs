using System.Collections.Generic;
using UnityEngine.Events;
using com.FDT.GameEvents;

namespace GarraTeam.TagsSystem
{
	public class TagsGameEventListener : GameEvent1Listener<List<TagAsset>, TagsGameEvent, TagsGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<List<TagAsset>>
		{
		}
	}
}
