using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	public class ColdDownCountGameEventListener : GameEvent2Listener<IDAsset, float, ColdDownCountGameEvent, ColdDownCountGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<IDAsset, float>
		{}
	}
}
