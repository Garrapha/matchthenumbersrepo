using UnityEngine;
using com.FDT.Common;

namespace com.FDT.GameEvents
{
	[CreateAssetMenu(menuName = "FDT/GameEvents/ColdDownCountGameEventGameEvent")]
	public class ColdDownCountGameEvent : GameEvent2<IDAsset, float>
	{
		public override string arg0label { get { return "ID"; } }
		public override string arg1label { get { return "miliSecond"; } }
	}
}
