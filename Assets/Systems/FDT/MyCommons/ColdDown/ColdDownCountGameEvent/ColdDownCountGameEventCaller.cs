using com.FDT.Common;

namespace com.FDT.GameEvents
{
	public class ColdDownCountGameEventCaller: GameEvent2Caller<IDAsset, float, ColdDownCountGameEvent>
	{
		[UnityEngine.ContextMenu("Invoke Raise Method (Runtime only)")]
		public void InvokeRaise()
		{
			Raise();
		}
	}
}
