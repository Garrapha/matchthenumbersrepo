using com.FDT.Common;
using UnityEditor;


namespace com.FDT.GameEvents.Editor
{
	[CustomEditor(typeof(ColdDownCountGameEvent))]
	public class ColdDownCountGameEventEditor : GameEvent2Editor<IDAsset, float>
	{
	}
}
