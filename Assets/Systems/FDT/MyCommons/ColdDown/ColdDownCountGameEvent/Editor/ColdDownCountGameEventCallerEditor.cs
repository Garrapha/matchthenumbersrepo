using UnityEditor;
using com.FDT.Common;

namespace com.FDT.GameEvents.Editor
{
	[CustomEditor(typeof(ColdDownCountGameEventCaller))]
	public class ColdDownCountGameEventCallerEditor : GameEvent2CallerEditor<IDAsset, float, ColdDownCountGameEvent>
	{
	}
}
