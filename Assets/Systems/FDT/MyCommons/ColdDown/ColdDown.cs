﻿using GarraTeam.Core;
using System;
using System.Threading.Tasks;

namespace MyCommons.ColdDown
{
    public static class ColdDown
    {
        public static async Task WaitAndCall(float secondsToWait, IDAssetGameEvent gameEvent)
        {
            await Task.Delay(TimeSpan.FromSeconds(secondsToWait));
            gameEvent.Raise();
        }
    }
}
