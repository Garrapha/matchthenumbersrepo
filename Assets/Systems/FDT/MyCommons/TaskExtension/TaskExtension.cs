﻿using System.Threading.Tasks;
namespace MyCommons.TaskExtension
{
    public static class TaskExtension
    {
        public static async void WrapError(this Task task)
        {
            await task;
        }
    }
}
