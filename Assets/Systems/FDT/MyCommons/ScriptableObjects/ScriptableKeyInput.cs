﻿using com.FDT.Common;
using UnityEngine;

[CreateAssetMenu(menuName = "GarraTeam/KeyInput", fileName = "keyName")]
public class ScriptableKeyInput : ScriptableObject
{
    [SerializeField] IDAsset key;

    public KeyCode Keycode
    {
        get
        {
            return (KeyCode)System.Enum.Parse(typeof(KeyCode), key.name);
        }
    }

    public IDAsset Key { get => key; private set => key = value; }
}
